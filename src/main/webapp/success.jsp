<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="com.training.web.model.Product" %>
<%@page import="com.training.web.model.Users" %>


<title>Insert title here</title>
</head>
<body>
<h1>Welcome ${userName}!</h1>

<%
	List<Product> prdList = (List<Product>)request.getAttribute("prodList");
	%>
	<table border="1">
	<tr>
	<th>ProdID</th>
	<th>ProdName</th>
	<th>ProdDesc</th>
	<th>Price</th>
	</tr>
	<%
		for(Product prd: prdList){%>
		<tr>
		<td><%=prd.getProdId()%></td>
		<td><%=prd.getProdName()%></td>
		<td><%=prd.getProdDesc()%></td>
		<td><%=prd.getPrice()%></td>
		</tr>
		<%} %>
		</table>
		
<%
	List<Users> usersList = (List<Users>)request.getAttribute("usersList");
	%>
	<table border="1">
	<tr>
	<th>UserName</th>
	<th>Password</th>
	</tr>
	<%
		for(Users usr: usersList){%>
		<tr>
		<td><%=usr.getUserName()%></td>
		<td><%=usr.getPassword()%></td>
		</tr>
		<%} %>
		</table>		
</body>
</html>