package com.training.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Product {

	private String prodId;
	private String prodName;
	private String prodDesc;
	private double price;
	
	
		
}
