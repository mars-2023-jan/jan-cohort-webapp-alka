package com.training.web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.training.web.model.Product;
import com.training.web.model.Users;

/**
 * Servlet implementation class LoginServlet
 * 
 * @param <User>
 */
@WebServlet("/login")
public class LoginServlet<User> extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userName = request.getParameter("uname");
		String password = request.getParameter("pwd");
		Connection con;
		RequestDispatcher rd = null;

		try {
			con = getDBConnection();
			List<Product> prodList = getProductValues(con, request);
			request.setAttribute("prodList", prodList);

			List<Users> usersList = getUsers(con, request);
			request.setAttribute("usersList", usersList);
			boolean isUserFound = false;

			for (Users usr : usersList) {
				if (usr.getUserName().equals(userName) && usr.getPassword().equals(password)) {
					isUserFound = true;
					request.setAttribute("userName", userName);
					break;
				} else {
					isUserFound = false;
					continue;
				}

			}
			if(isUserFound)
				rd = request.getRequestDispatcher("success.jsp");
			else
			rd = request.getRequestDispatcher("failure.jsp");

			rd.forward(request, response);


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<Users> getUsers(Connection con, HttpServletRequest request) throws SQLException {
		PreparedStatement pstmt = con.prepareStatement("Select * from user");
		ResultSet rs = pstmt.executeQuery();
		List<Users> usersList = new ArrayList<Users>();

		while (rs.next()) {
			Users usr = new Users();
			usr.setUserName(rs.getString(1));
			usr.setPassword(rs.getString(2));

			usersList.add(usr);
		}
		return usersList;
	}

	private List<Product> getProductValues(Connection con, HttpServletRequest request) throws SQLException {
		PreparedStatement pstmt = con.prepareStatement("Select * from product");
		ResultSet rs = pstmt.executeQuery();
		List<Product> prodList = new ArrayList<Product>();

		while (rs.next()) {
			Product prd = new Product();
			prd.setProdId(rs.getString(1));
			prd.setProdName(rs.getString(2));
			prd.setProdDesc(rs.getString(3));
			prd.setPrice(rs.getDouble(4));

			prodList.add(prd);
		}
		return prodList;
	}

	private Connection getDBConnection() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/mars_jan";

		String user = "root";
		String password = "Alka@123";

		Connection con = DriverManager.getConnection(url, user, password);
		return con;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

//Create a user table in mysql database with username and password fields
//fetch the username and password from the database and vaildate the user